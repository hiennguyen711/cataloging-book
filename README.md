# catalog

> Cataloging book project

## Cataloging book
+ User input the ISBN number to the field and press Search / or press Enter on the keyboard to launch the API lookup
+ Found data is input to the rest of the fields
+ Nothing is found and inform messaging is displayed


## Finna API

## Using:
+ HTML
+ CSS
+ VueJS
+ NodeJS
+ Simple Testing: Karma, PhantomJS


## Screenshots of demo

+ <img src="./screenshots/1.png" />
+ <img src="./screenshots/2.png" />
+ <img src="./screenshots/3.png" />
+ <img src="./screenshots/4.png" />




## Screenshots of test results

+ <img src="./screenshots/test.png" />





## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
