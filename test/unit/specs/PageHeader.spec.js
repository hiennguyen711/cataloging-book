import Vue from 'vue';

import PageHeader from '@/components/PageHeader'

describe('PageHeader.vue', () => {

  it(`should update when dataText is changed.`, done => {
    const Constructor = Vue.extend(PageHeader);

    const comp = new Constructor().$mount();

    comp.title = 'New Text';

    Vue.nextTick(() => {
      expect(comp.$el.textContent)
        .to.equal('New Text');


    });
    done();
  });
});
