import Vue from 'vue'
import PageFooter from '@/components/PageFooter'

describe('PageFooter.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(PageFooter)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.page--footer .footer .container p strong').textContent)
      .to.equal('Cataloging Book')
  })
})
