// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
require('es6-promise').polyfill()
import Vue from 'vue'
import App from './App'
import router from './router'
import Buefy from 'buefy';
import store from '../src/vuex/store'
import moment from 'moment'

Vue.config.productionTip = false
Vue.use(Buefy);

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('YYYY')
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
