import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  //state
  state: {
    book: {}
  },
  actions: {
    LOAD_BOOK: ({ commit }, isbn) => {
      axios.get(`https://api.finna.fi/api/v1/search?lookfor=${isbn}&type=isbn&field[]=isbn&field[]=title&field[]=authors&field[]=year&field[]=publishers&sort=relevance%2Cid%20asc&page=1&limit=1&prettyPrint=1&lng=fi`, { showProgressBar: false })
        .then(res => {
          // let result = res.data.records[0] ?  res.data.records[0] : {"noBook":true}
          let result = res.data

          commit('SET_BOOK', { book: result })
        }, (err) => {
          console.log(err)
        })
    }
  },
  mutations: {
    SET_BOOK: (state, { book }) => {
      state.book = book
    }
  },
  getters: {
    listBook: state => {
      return state.book
    }
  },
  modules: {

  }
})
export default store
